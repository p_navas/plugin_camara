import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { ProductoService } from '../producto.service'
import { CommonModule} from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  standalone: true,
  imports: [IonicModule, ExploreContainerComponent, CommonModule],
  providers: [ProductoService]
})
export class Tab2Page {

  productos=[]
  


  constructor(private servicio:ProductoService, private router:Router) {}

  ionViewWillEnter() {
    this.servicio.traer_productos().subscribe(respuesta=>{
      console.log(respuesta)
      this.productos = respuesta
    })
  }



  modificar(codigo_producto: number, nombre:string, descripcion:string, precio:number){
    console.log(codigo_producto)
    this.router.navigate(["actualizar"], {queryParams:{
      codigo: codigo_producto,
      nombre: nombre,
      descripcion: descripcion,
      precio:precio
    }})
  }

  eliminar(codigo: number){
    this.servicio.eliminar_producto(codigo).subscribe(respuesta=>{
      console.log(respuesta)
      window.location.reload() // recarga la pantalla
    })
  }


}
