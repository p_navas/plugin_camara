import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { AlertController, ToastController, ToastOptions } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { ProductoService } from '../producto.service'
import { CommonModule, NgFor, NgIf } from '@angular/common';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, ExploreContainerComponent, FormsModule],
  providers:[Camera, ProductoService]
})
export class Tab1Page {
  constructor(private camara:Camera, private servicio:ProductoService, 
              private alerta:AlertController, private toast: ToastController) {}

  foto:string =""
  nombre=""
  descripcion=""
  precio=0.0
  disableEnviarBtn = true

  customCounterFormatter(inputLength: number, maxLength: number) {
    return `${maxLength - inputLength} caracteres disponibles`;
  }

  validarEnvio(){
    if (this.foto == "" || this.nombre == "" || this.descripcion == "" || this.precio == 0.0){
      this.disableEnviarBtn = true
      
    }else if(this.foto != "" && this.nombre != "" && this.descripcion != "" && this.precio != 0.0){
      this.disableEnviarBtn = false
    }
    return this.disableEnviarBtn
  }

 async mensajeResultadoServidor(mensaje:string){
 
    const alerta = await this.toast.create({
      message:  mensaje,
      duration: 2000,
      position: "middle",
      animated: true,
      color: "danger"
    })

    alerta.present()
    
  }

  crearAlerta(){
    const alerta_enviar = this.alerta.create({
      header: "¿Enviar datos?",
      buttons: [{
        text: "Cancelar",
        role: "cancel"
      },
      {
        text: "Aceptar",
        role:"confirm",
        handler: ()=>{
          this.enviar()
        }
      }
      ]
    })
  }


  tomarFoto(){
    const opcionesCamara:CameraOptions = {
      quality: 100,
      destinationType: this.camara.DestinationType.DATA_URL,
      encodingType: this.camara.EncodingType.JPEG,
      mediaType:this.camara.MediaType.PICTURE,
      cameraDirection: this.camara.Direction.BACK,
      correctOrientation: true
      //targetHeight: 40,
      //targetWidth: 40
      //sourceType: this.camara.PictureSourceType.
    }

    this.camara.getPicture(opcionesCamara).then(imagen=>{
      let base64Image = 'data:image/jpeg;base64,' + imagen;
      this.foto = base64Image
      //alert(this.foto)
    }, err=>{
      alert("Cámara cancelada")
    })
  }


  cambiarFoto(){
   if (this.validarEnvio() == true){
    this.disableEnviarBtn = true
   }else{
    this.disableEnviarBtn = false
   }
  }



  enviar(){
    let datos = {
      "nombre": this.nombre,
      "descripcion": this.descripcion,
      "precio" : this.precio,
      "imagen": this.foto.split(",")[1]
    }
    this.servicio.agregar_producto(datos).subscribe((respuesta)=>{
      console.log(respuesta)
      this.cancelar()
      this.mensajeResultadoServidor(respuesta.toString())
    }, (error)=>{
      //alert("error: " + error.status + ", " + error.statusText)
      this.mensajeResultadoServidor(error.statusText)
    });
    
  }

  cancelar(){
    this.foto = ""
    this.nombre =""
    this.precio = 0.0
    this.descripcion = ""
  }




}
