import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  url:string = "https://pnavas.pythonanywhere.com/"

  constructor(private http:HttpClient) { }

  agregar_producto(datos:any){
    let headers = new HttpHeaders()
    headers.append("Content-Type", "application/json");
    return this.http.post(this.url + "post_producto", datos, {headers:headers})
  
  }

  traer_productos(){
    return this.http.get<[]>(this.url + "traer_productos")
  }

  eliminar_producto(codigo:number){
    return this.http.delete(this.url + "eliminar_producto/" + codigo)
  }


  acutalizar_producto(codigo:number, datos:any){
    return this.http.put(this.url + "actualizar_producto/" + codigo, datos)
  }


}
